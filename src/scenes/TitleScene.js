// @ts-nocheck
import Phaser from "phaser";
import colors from "../utils/colors.json";

export default class TitleScene extends Phaser.Scene {
    constructor() {
        super("title");
    }

    preload() {
        this.load.image("background", "Background.jpg");
    }

    create() {
        this.input.setDefaultCursor("url(cursor.png), pointer");
        const background = this.add.image(
            this.scale.width / 2,
            this.scale.height / 2,
            "background"
        );
        const titleText = this.add.text(
            this.scale.width / 2,
            140,
            "Wizard Command",
            {
                fontFamily: "kalam",
                align: "center",
                fontSize: 160,
                color: colors.purple,
            }
        );
        titleText.setOrigin(0.5);

        // localStorage.removeItem("wizardCommandHighScore");
        let highScore = localStorage.getItem("wizardCommandHighScore") || 0;

        const highScoreText = this.add.text(
            this.scale.width / 2,
            300,
            `High Score: ${highScore}`,
            {
                fontFamily: "kalam",
                align: "center",
                fontSize: 80,
                color: colors.blue,
            }
        );
        highScoreText.setOrigin(0.5);

        const startButton = this.add
            .text(this.scale.width / 2, 550, "Start", {
                fontFamily: "kalam",
                align: "center",
                fontSize: 120,
                color: colors.orange,
            })
            .setInteractive();
        startButton.setOrigin(0.5);

        startButton.on("pointerover", () => {
            startButton.setColor(colors.yellow);
        });

        startButton.on("pointerout", () => {
            startButton.setColor(colors.orange);
        });

        startButton.on("pointerdown", () => {
            startButton.setColor(colors.blue);
            setTimeout(() => {
                this.scene.start("main");
            }, 50);
        });

        // const tutorialButton = this.add
        //     .text(this.scale.width / 2, 740, "Tutorial", {
        //         fontFamily: "kalam",
        //         align: "center",
        //         fontSize: 120,
        //         color: colors.orange,
        //     })
        //     .setInteractive();
        // tutorialButton.setOrigin(0.5);

        // tutorialButton.on("pointerover", () => {
        //     tutorialButton.setColor(colors.yellow);
        // });

        // tutorialButton.on("pointerout", () => {
        //     tutorialButton.setColor(colors.orange);
        // });

        // tutorialButton.on("pointerdown", () => {
        //     tutorialButton.setColor(colors.blue);
        //     setTimeout(() => {
        //         this.scene.start("tutorial");
        //     }, 50);
        // });
    }
}
