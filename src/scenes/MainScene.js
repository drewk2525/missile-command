// @ts-nocheck
import Phaser from "phaser";
import { generateUnit } from "../utils/createEnemies";
import {
    allWizardsDead,
    createWizards,
    drawCooldown,
    drawDeepFreezeCooldown,
    wizardAbilityCollisions,
    wizardCollisions,
} from "../utils/createWizards";
import { getEnemies } from "../utils/enemyUnits";
import { endGame } from "../utils/util";

const rng = new Phaser.Math.RandomDataGenerator();

// let speedMultipler = 1;

export class MainScene extends Phaser.Scene {
    constructor() {
        super("main");
    }

    preload() {
        this.paused = false;
        this.spawnTime = 1.5;
        this.baseSpeed = 1.1;
        this.baseEnemySpeed = 1;
        this.unitsSpawned = 0;
        this.score = 0;
        this.load.image("background", "Background.jpg");
        const wizardConfig = {
            frameWidth: 31,
            frameHeight: 31,
            endFrame: 20,
        };
        const upgradeConfig = {
            frameWidth: 32,
            frameHeight: 32,
            endFrame: 20,
        };
        this.load.image("snowflake", "snowflake.png");
        this.load.image("deepFreeze", "deepFreeze.png");
        this.load.image("upgradeOrb", "upgradeOrb.png");
        this.load.spritesheet("cooldown", "cooldown.png", upgradeConfig);
        this.load.spritesheet(
            "projectileSpeed",
            "projectileSpeed.png",
            upgradeConfig
        );
        this.load.spritesheet(
            "projectileSize",
            "projectileSize.png",
            upgradeConfig
        );
        this.load.spritesheet(
            "projectileNumber",
            "projectileNumber.png",
            upgradeConfig
        );
        this.load.spritesheet("resurrect", "resurrect.png", upgradeConfig);
        this.load.spritesheet("blueWizard", "blueWizard.png", wizardConfig);
        this.load.spritesheet("redWizard", "redWizard.png", wizardConfig);
        this.load.spritesheet("orangeWizard", "orangeWizard.png", wizardConfig);
        this.load.spritesheet("purpleWizard", "purpleWizard.png", wizardConfig);
        this.load.spritesheet("beholder", "beholder.png", {
            frameWidth: 64,
            frameHeight: 64,
            endFrame: 20,
        });
        this.load.spritesheet("beholderAcid", "beholderAcid.png", {
            frameWidth: 16,
            frameHeight: 16,
            endFrame: 20,
        });
        this.load.spritesheet("bat", "bat.png", {
            frameWidth: 32,
            frameHeight: 16,
            endFrame: 20,
        });
        this.load.spritesheet("imp", "imp.png", {
            frameWidth: 32,
            frameHeight: 32,
            endFrame: 20,
        });
        this.load.spritesheet("purpleMainAttack", "purpleMainAttack.png", {
            frameWidth: 32,
            frameHeight: 32,
            endFrame: 20,
        });
        this.load.spritesheet("orangeMainAttack", "orangeMainAttack.png", {
            frameWidth: 32,
            frameHeight: 32,
            endFrame: 20,
        });
        this.load.spritesheet("redMainAttack", "redMainAttack.png", {
            frameWidth: 32,
            frameHeight: 32,
            endFrame: 20,
        });
        this.load.spritesheet("snowball", "snowball.png", {
            frameWidth: 32,
            frameHeight: 32,
            endFrame: 20,
        });

        this.load.audio("blueAttack", ["sfx/blueAttack.wav"]);
        this.load.audio("orangeAttack", ["sfx/orangeAttack.wav"]);
        this.load.audio("orangeSplit", ["sfx/orangeSplit.wav"]);
        this.load.audio("redAttack", ["sfx/redAttack.wav"]);
        this.load.audio("purpleAttack", ["sfx/purpleAttack.wav"]);
        this.load.audio("wizardDeath", ["sfx/wizardDeath.wav"]);
        this.load.audio("upgrade", ["sfx/upgrade.wav"]);
        this.load.audio("resurrect", ["sfx/resurrect.wav"]);
        this.load.audio("splat", ["sfx/splat.wav"]);
        this.load.audio("smallSplat", ["sfx/smallSplat.wav"]);
        this.load.audio("impDeath", ["sfx/impDeath.wav"]);
        this.load.audio("poof", ["sfx/poof.wav"]);
    }

    create() {
        const volume = 0.8;
        const blueAttack = this.sound.add("blueAttack");
        const orangeAttack = this.sound.add("orangeAttack");
        const orangeSplit = this.sound.add("orangeSplit");
        const redAttack = this.sound.add("redAttack");
        const purpleAttack = this.sound.add("purpleAttack");
        const wizardDeath = this.sound.add("wizardDeath");
        const upgrade = this.sound.add("upgrade");
        const resurrect = this.sound.add("resurrect");
        const splat = this.sound.add("splat");
        const smallSplat = this.sound.add("smallSplat");
        const impDeath = this.sound.add("impDeath");
        const poof = this.sound.add("poof");

        this.sfx = {
            blueAttack: () => {
                blueAttack.play();
                blueAttack.setVolume(volume * 2);
            },
            orangeAttack: () => {
                orangeAttack.play();
                orangeAttack.setVolume(volume * 3);
            },
            orangeSplit: () => {
                orangeSplit.play();
                orangeSplit.setVolume(volume * 3);
            },
            redAttack: () => {
                redAttack.play();
                redAttack.setVolume(volume * 0.6);
            },
            purpleAttack: () => {
                purpleAttack.play();
                purpleAttack.setVolume(volume);
            },
            wizardDeath: () => {
                wizardDeath.play();
                wizardDeath.setVolume(volume * 2);
            },
            upgrade: () => {
                upgrade.play();
                upgrade.setVolume(volume * 0.6);
            },
            resurrect: () => {
                resurrect.play();
                resurrect.setVolume(volume * 0.6);
            },
            splat: () => {
                splat.play();
                splat.setVolume(volume);
            },
            smallSplat: () => {
                smallSplat.play();
                smallSplat.setVolume(volume * 1.5);
            },
            impDeath: () => {
                impDeath.play();
                impDeath.setVolume(volume);
            },
            poof: () => {
                poof.play();
                poof.setVolume(volume);
            },
        };

        this.input.setDefaultCursor("url(cursor.png), pointer");
        this.add.image(
            this.scale.width / 2,
            this.scale.height / 2,
            "background"
        );
        // Cooldowns
        const q = 5;
        const w = 1.6;
        const e = 7;
        const r = 10;
        const baseWizard = {
            alive: true,
            numberUpgrades: 0,
        };
        this.wizards = [
            {
                ...baseWizard,
                x: 350,
                button: "Q",
                color: "orange",
                baseAttackSpeed: 600,
                baseProjectileSize: 1.2,
                baseProjectileNumber: 10,
                baseCooldown: q - (q * this.baseSpeed - q),
            },
            {
                ...baseWizard,
                x: 725,
                button: "W",
                color: "purple",
                baseAttackSpeed: 1200,
                baseProjectileSize: 1,
                baseProjectileNumber: 1,
                baseCooldown: w - (w * this.baseSpeed - w),
            },
            {
                ...baseWizard,
                x: 1110,
                button: "E",
                color: "red",
                baseAttackSpeed: 800,
                baseProjectileSize: 1.6,
                baseProjectileNumber: 3,
                baseCooldown: e - (e * this.baseSpeed - e),
            },
            {
                ...baseWizard,
                x: 1490,
                button: "R",
                color: "blue",
                baseAttackSpeed: 2500,
                baseProjectileSize: 1,
                baseProjectileNumber: 0,
                baseCooldown: r - (r * this.baseSpeed - r),
            },
        ];
        this.scoreText = this.add.text(30, 20, this.score, {
            fontFamily: "kalam",
            align: "center",
            fontSize: 120,
            color: "#ffffff",
        });

        /**
         * Create Wizards
         */
        createWizards(this);

        /**
         * Create Enemies
         */
        this.enemies = getEnemies(this);
        this.availableEnemies = ["bat"];
        this.availableUpgrades = [
            "projectileSize",
            "projectileSpeed",
            "projectileNumber",
            "cooldown",
        ];
        this.enemyGroup = this.physics.add.group({
            key: [
                "bat",
                "imp",
                "cooldown",
                "projectileSize",
                "projectileNumber",
                "projectileSpeed",
                "resurrect",
            ],
            max: 0,
            active: false,
            visible: false,
            setXY: {
                x: -10000,
                y: -10000,
            },
        });
        Object.entries(this.enemies).forEach(([_key, value]) => {
            this.anims.create({
                key: `${value.texture}Default`,
                frames: this.anims.generateFrameNumbers(value.texture, {
                    start: 0,
                    end: 2,
                }),
                repeat: -1,
                frameRate: value.frameRate,
                yoyo: true,
            });
            this.anims.create({
                key: `${value.texture}Death`,
                frames: this.anims.generateFrameNumbers(value.texture, {
                    start: 3,
                    end: 5,
                }),
                repeat: 0,
                frameRate: value.deathFrameRate,
                yoyo: value.deathYoyo,
            });
        });

        /**
         * Collisions
         */
        wizardCollisions(this);
        wizardAbilityCollisions(this);

        const esc = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.ESC
        );
        esc.on("down", () => {
            // this.pausedTime = parseInt(this.time.now);
            this.time.scale = 0;
            this.scene.pause();
            this.scene.launch("paused");
        });
        this.events.on("resume", () => {
            this.time.scale = 1;
        });
    }

    getRandomWizard() {
        let target;
        do {
            const i = rng.between(0, this.wizards.length - 1);
            target = this.wizards[i];
        } while (!target.alive && !allWizardsDead(this));
        return target;
    }

    update() {
        if (allWizardsDead(this) && !this.endgameBackground) {
            endGame(this);
        }
        if (this.boss && this.boss.state !== "death" && this.boss.y > 300) {
            this.boss.metaData.movement(this.boss);
        }
        this.wizardAbilityGroup.children.entries.forEach((child) => {
            if (
                child.x < 0 ||
                child.y < 0 ||
                child.x > this.scale.width ||
                child.y > this.scale.height
            ) {
                if (child.buttonTextTween) {
                    child.buttonTextTween.destroy();
                }
                if (child.wizard) {
                    child.wizard.activeAttack = null;
                }
                child.destroy();
            }
        });
        if (!this.startTime) {
            this.startTime = this.time.now;
        }
        const currentTime = this.time.now - this.startTime;
        if (!this.timeDiff) {
            this.timeDiff = 0;
        }
        if (!this.timeCheckpoint) {
            this.timeCheckpoint = 0;
        }
        this.timeDiff = currentTime - this.timeCheckpoint;
        this.timeCheckpoint = currentTime;
        if (!this.spawnTimer || this.spawnTimer <= 0) {
            this.spawnTimer =
                (this.spawnTime / (this.baseSpeed * this.baseEnemySpeed)) *
                1000;
            if (!this.availableEnemies.includes("imp") && this.score >= 10) {
                this.availableEnemies.push("imp");
            }
            const i = rng.between(0, this.availableEnemies.length - 1);
            if (this.bossFight) {
                this.boss.metaData.spawnUnits(this);
            } else {
                if (this.unitsSpawned % 51 == 0 && this.unitsSpawned > 0) {
                    this.boss = generateUnit(this, this.enemies.beholder);
                    this.bossFight = true;
                } else if (
                    this.unitsSpawned % 10 === 0 &&
                    this.unitsSpawned > 0
                ) {
                    const r = rng.between(0, this.availableUpgrades.length - 1);
                    generateUnit(this, this.enemies[this.availableUpgrades[r]]);
                } else {
                    generateUnit(this, this.enemies[this.availableEnemies[i]]);
                }
            }
        }
        this.spawnTimer -= this.timeDiff;
        this.wizards.forEach((wizard) => {
            wizard.cooldownTimer -= this.timeDiff;
            if (wizard.cooldownTimer <= 0) {
                wizard.cooldownTimer = null;
            }
            drawCooldown(this, wizard);
        });
        if (this.deepFreezeCooldown) {
            this.deepFreezeCooldown -= this.timeDiff;
        }
        drawDeepFreezeCooldown(this);
    }

    updateScore(score) {
        this.score += score;
        this.scoreText.setText(this.score.toString());
    }
}

export class PausedScene extends Phaser.Scene {
    constructor() {
        super("paused");
    }

    create() {
        const rectangle = this.add.rectangle(
            0,
            0,
            this.scale.width,
            this.scale.height,
            0x555555,
            0.5
        );
        rectangle.setOrigin(0);
        const textProps = {
            fontFamily: "kalam",
            align: "center",
            fontSize: 120,
            color: "#ffffff",
        };
        const text = this.add.text(
            this.scale.width / 2,
            this.scale.height / 2,
            "Paused",
            textProps
        );
        text.setOrigin(0.5);
        const esc = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.ESC
        );
        esc.on("down", () => {
            this.scene.resume("main");
            this.scene.stop();
        });
    }

    update() {}
}
