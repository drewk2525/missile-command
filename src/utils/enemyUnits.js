import { generateUnit } from "./createEnemies";

const rng = new Phaser.Math.RandomDataGenerator();
export const getEnemies = (scene) => {
    const beholderMovement = (newUnit) => {
        if (newUnit.state === "death") {
            return;
        }
        if (newUnit.state === "frozen") {
            setTimeout(() => {
                beholderMovement(newUnit);
            }, 200);
            return;
        }
        const y = newUnit.y > 150 ? 100 : 200;
        const rand = newUnit.x < 900 ? 900 : 100;
        newUnit.body.collideWorldBounds = true;
        newUnit.body.bounce.setTo(0.9, 0.9);
        newUnit.currentMovement = scene.physics.moveTo(
            newUnit,
            rng.between(rand, rand + 900),
            y,
            newUnit.metaData.speed * scene.baseSpeed
        );
        setTimeout(() => {
            beholderMovement(newUnit);
        }, rng.between(2500, 4000));
    };

    const enemies = {
        bat: {
            texture: "bat",
            frameRate: 6,
            deathFrameRate: 24,
            deathSound: "poof",
            deathYoyo: true,
            scale: 2.8,
            speed: 180,
            points: 1,
            hitpoints: 1,
        },
        imp: {
            texture: "imp",
            frameRate: 8,
            deathFrameRate: 16,
            deathSound: "impDeath",
            deathYoyo: true,
            scale: 2.8,
            speed: 225,
            points: 3,
            hitpoints: 1,
            size: { x: 32, y: 24 },
            offset: { x: 0, y: 0 },
            deathCallback: (self) => {},
            createCallback: (self) => {
                const rand = rng.between(4000, 4500);
                setTimeout(() => {
                    if (
                        self.sprite.state === "frozen" ||
                        self.sprite.state === "death"
                    ) {
                        return;
                    }
                    const target = scene.getRandomWizard();
                    scene.physics.moveTo(
                        self.sprite,
                        target.sprite.x,
                        target.sprite.y,
                        self.speed * scene.baseSpeed
                    );
                }, rand);
            },
        },
        beholder: {
            texture: "beholder",
            isBoss: true,
            frameRate: 4,
            deathFrameRate: 6,
            deathSound: "splat",
            deathYoyo: false,
            scale: 2.8,
            speed: 180,
            points: 100,
            hitpoints: 10,
            size: { x: 52, y: 52 },
            offset: { x: 6, y: 6 },
            deathCallback: (self, wizard) => {
                self.scene.bossFight = false;
            },
            movement: beholderMovement,
            spawnUnits: (scene) => {
                generateUnit(scene, scene.enemies.beholderAcid);
            },
        },
        beholderAcid: {
            texture: "beholderAcid",
            frameRate: 6,
            deathFrameRate: 24,
            deathSound: "smallSplat",
            deathYoyo: true,
            scale: 2.8,
            speed: 180,
            points: 0,
            hitpoints: 1,
            deathIncrement: 0,
            freezeImmune: true,
        },
    };

    const baseUpgrade = {
        frameRate: 4,
        deathFrameRate: 6,
        deathSound: "upgrade",
        deathYoyo: false,
        scale: 1.5,
        speed: 150,
        points: 10,
        isUpgrade: true,
        hitpoints: 1,
    };

    const upgrades = {
        cooldown: {
            texture: "cooldown",
            text: "Cooldown Speed",
            ...baseUpgrade,
            deathCallback: (_enemy, wizard) => {
                wizard.baseCooldown =
                    wizard.baseCooldown - wizard.baseCooldown * 0.1;
            },
        },
        projectileSpeed: {
            texture: "projectileSpeed",
            text: "Projectile Speed",
            ...baseUpgrade,
            deathCallback: (_enemy, wizard) => {
                wizard.baseAttackSpeed =
                    wizard.baseAttackSpeed + wizard.baseAttackSpeed * 0.1;
            },
        },
        projectileSize: {
            texture: "projectileSize",
            text: "Projectile Size",
            ...baseUpgrade,
            deathCallback: (_enemy, wizard) => {
                wizard.baseProjectileSize =
                    wizard.baseProjectileSize + wizard.baseProjectileSize * 0.1;
            },
        },
        projectileNumber: {
            texture: "projectileNumber",
            text: "Additional Projectiles",
            ...baseUpgrade,
            deathCallback: (_enemy, wizard) => {
                if (wizard.color === "orange") {
                    wizard.baseProjectileNumber += 2;
                } else {
                    wizard.baseProjectileNumber += 1;
                }
            },
        },
        resurrect: {
            ...baseUpgrade,
            texture: "resurrect",
            text: "Resurrect Allies",
            deathSound: "resurrect",
            speed: 250,
            points: 25,
            deathCallback: (enemy, _wizard) => {
                enemy.scene.wizards.forEach((wizard) => {
                    console.log({ wizard });
                    if (!wizard.alive) {
                        wizard.alive = true;
                        wizard.cooldownTimer = 0;
                        wizard.sprite.playReverse(`${wizard.color}Death`);
                    }
                });
            },
        },
    };

    return { ...enemies, ...upgrades };
};
