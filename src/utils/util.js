// @ts-nocheck
import colors from "./colors.json";
export const getCursorCenter = (scene) => {
    const offset = 22;
    return {
        x: scene.input.x + offset,
        y: scene.input.y + offset,
    };
};

export const endGame = (scene) => {
    scene.input.setDefaultCursor("url(cursor_light.png), pointer");
    scene.scoreText.depth = 10;
    scene.scoreText.setText(`Your Score: ${scene.score}`);
    scene.endgameBackground = scene.add.rectangle(
        0,
        0,
        scene.scale.width,
        scene.scale.height,
        0x000000
    );
    scene.scoreText.setOrigin(0.5);
    scene.scoreText.x = scene.scale.width / 2;
    scene.scoreText.y = 100;

    let highScore = localStorage.getItem("wizardCommandHighScore") || 0;
    const textProps = {
        fontFamily: "kalam",
        align: "center",
        fontSize: 120,
        color: colors.blue,
    };
    let newHighScoreText, previousBestText;
    if (scene.score > highScore) {
        newHighScoreText = scene.add.text(
            scene.scale.width / 2,
            400,
            "New High Score!!",
            {
                ...textProps,
                color: colors.red,
            }
        );
        newHighScoreText.setOrigin(0.5);
        scene.add.tween({
            targets: newHighScoreText,
            duration: 600,
            repeat: -1,
            alpha: 0.5,
            yoyo: true,
            onComplete: () => {
                newHighScoreText.destroy();
            },
        });
        previousBestText = scene.add.text(
            scene.scale.width / 2,
            510,
            `Previous Best: ${highScore}`,
            {
                ...textProps,
                fontSize: 40,
                color: colors.yellow,
            }
        );
        previousBestText.setOrigin(0.5);
        highScore = scene.score;
        localStorage.setItem("wizardCommandHighScore", highScore);
    }

    const highScoreText = scene.add.text(
        scene.scale.width / 2,
        250,
        `High Score: ${highScore}`,
        textProps
    );
    highScoreText.setOrigin(0.5);

    scene.endgameBackground.setOrigin(0);
    scene.endgameBackground.alpha = 0.5;

    const startButton = scene.add
        .text(scene.scale.width / 2, 650, "Play Again", {
            fontFamily: "kalam",
            align: "center",
            fontSize: 120,
            color: colors.orange,
        })
        .setInteractive();
    startButton.setOrigin(0.5);

    startButton.on("pointerover", () => {
        startButton.setColor(colors.yellow);
    });

    startButton.on("pointerout", () => {
        startButton.setColor(colors.orange);
    });

    startButton.on("pointerdown", () => {
        startButton.setColor(colors.blue);
        setTimeout(() => {
            highScoreText.destroy();
            if (newHighScoreText) {
                previousBestText.destroy();
                newHighScoreText.destroy();
            }
            scene.boss = null;
            scene.bossFight = false;
            scene.endgameBackground.destroy();
            scene.endgameBackground = null;
            scene.scene.start("main");
        }, 50);
    });

    const endGameButton = scene.add
        .text(scene.scale.width / 2, 850, "Quit", {
            fontFamily: "kalam",
            align: "center",
            fontSize: 120,
            color: colors.red,
        })
        .setInteractive();
    endGameButton.setOrigin(0.5);

    endGameButton.on("pointerover", () => {
        endGameButton.setColor(colors.yellow);
    });

    endGameButton.on("pointerout", () => {
        endGameButton.setColor(colors.red);
    });

    endGameButton.on("pointerdown", () => {
        endGameButton.setColor(colors.blue);
        setTimeout(() => {
            highScoreText.destroy();
            if (newHighScoreText) {
                previousBestText.destroy();
                newHighScoreText.destroy();
            }
            scene.boss = null;
            scene.bossFight = false;
            scene.endgameBackground.destroy();
            scene.endgameBackground = null;
            scene.scene.start("title");
        }, 50);
    });
};
