// @ts-nocheck
import { addWizardUpgrade, allWizardsDead } from "./createWizards";

import colors from "./colors.json";
const rng = new Phaser.Math.RandomDataGenerator();

export const enemyDie = (enemySprite, wizard) => {
    enemySprite.currentHitpoints -= 1;
    if (enemySprite.currentHitpoints > 0) {
        return;
    }
    const scene = enemySprite.scene;
    if (enemySprite.state === "death" || allWizardsDead(scene)) {
        return;
    }
    const points = enemySprite.metaData.points;
    if (wizard) {
        scene.updateScore(points);
        if (points > 0) {
            const text = scene.add.text(
                enemySprite.x,
                enemySprite.y,
                `+${points}`,
                {
                    fontFamily: "kalam",
                    align: "center",
                    fontSize: 50,
                    color: "#ffffff",
                }
            );
            scene.add.tween({
                targets: text,
                x: 100,
                y: 100,
                duration: 500,
                alpha: 0,
                repeat: 0,
                ease: "Quint.easeIn",
                onComplete: () => {
                    text.destroy();
                },
            });
        }
        if (enemySprite.metaData.deathCallback) {
            enemySprite.metaData.deathCallback(enemySprite, wizard);
        }
    }
    if (
        enemySprite.metaData.isUpgrade &&
        wizard &&
        wizard.numberUpgrades < 10
    ) {
        const text = scene.add.text(
            enemySprite.x,
            enemySprite.y,
            enemySprite.metaData.text,
            {
                fontFamily: "kalam",
                align: "center",
                fontSize: 40,
                color: colors.blue,
            }
        );
        addWizardUpgrade(enemySprite, wizard);
        text.setOrigin(0.5);
        scene.add.tween({
            targets: text,
            x: wizard.sprite.x,
            y: wizard.sprite.y,
            duration: 1100,
            alpha: 0.4,
            repeat: 0,
            ease: "Cubic.easeIn",
            onComplete: () => {
                text.destroy();
            },
        });
    }
    enemySprite.state = "death";
    scene.sfx[enemySprite.metaData.deathSound || "splat"]();
    if (enemySprite.name === "resurrect") {
        console.log("Playing resurrection anmiation");
        console.log(enemySprite);
    }
    enemySprite.play(`${enemySprite.name}Death`);
    if (enemySprite.metaData.isBoss) {
        scene.bossFight = false;
        scene.boss = null;
        generateUnit(scene, scene.enemies.resurrect);
    }
    enemySprite.on("animationcomplete", () => {
        enemySprite.state = 0;
        enemySprite.destroy();
    });
};

export const generateUnit = (scene, unit) => {
    if (unit.texture === "resurrect") {
        console.log("resurrect");
        console.log(unit);
    }
    if (allWizardsDead(scene)) {
        return;
    }
    let x, y;
    if (scene.bossFight) {
        x = scene.boss.x;
        y = scene.boss.y;
    } else {
        x = rng.between(100, scene.scale.width - 100);
        y = -50;
    }
    const newUnit = scene.enemyGroup.create(x, y, unit.texture, 0, true, true);
    unit.sprite = newUnit;
    newUnit.body.immovable = true;
    if (unit.size) {
        newUnit.body.setSize(unit.size.x, unit.size.y);
    }
    if (unit.offset) {
        newUnit.body.setOffset(unit.offset.x, unit.offset.y);
    }
    newUnit.currentHitpoints = unit.hitpoints;
    newUnit.name = unit.texture;
    newUnit.setScale(unit.scale);
    newUnit.play(`${unit.texture}Default`);
    newUnit.metaData = scene.enemies[unit.texture];

    if (unit.isBoss) {
        newUnit.currentHitpoints += Math.floor(scene.unitsSpawned / 50) * 5;
        scene.boss = unit;
        unit.movement(newUnit);
    } else {
        const target = scene.getRandomWizard();
        newUnit.currentMovement = scene.physics.moveTo(
            newUnit,
            target.sprite.x,
            target.sprite.y,
            unit.speed * scene.baseSpeed
        );
    }
    if (unit.createCallback) {
        unit.createCallback(unit);
    }
    scene.unitsSpawned +=
        unit.deathIncrement !== undefined ? unit.deathIncrement : 1;
    scene.spawnTime = scene.spawnTime * 0.999;
    scene.baseEnemySpeed = scene.baseEnemySpeed * 1.00005;
    const minSpawnTime = 0.6;
    if (scene.spawnTime < minSpawnTime) {
        scene.spawnTime = minSpawnTime;
    }
    if (
        scene.unitsSpawned === 10 ||
        scene.unitsSpawned === 30 ||
        scene.unitsSpawned === 100
    ) {
        scene.availableEnemies.push("imp");
    }
    return newUnit;
};
