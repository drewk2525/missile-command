// @ts-nocheck
import colors from "./colors.json";
import { enemyDie } from "./createEnemies";
import { getCursorCenter } from "./util";

/**
 * TODO
 * orange ball explodes when on cooldown and you press the button again
 */

export const createWizards = (scene) => {
    const textProps = {
        fontFamily: "kalam",
        align: "center",
        fontSize: 120,
    };

    scene.wizards.forEach((wizard) => {
        const rect = scene.add.rectangle(
            wizard.x - 10,
            790,
            120,
            120,
            0x555555
        );
        rect.setOrigin(0);
        const upgradeRect = scene.add.rectangle(
            wizard.x - 10,
            920,
            120,
            45,
            0x333333
        );
        upgradeRect.setOrigin(0);
        const xOffset = wizard.button == "W" ? 0 : 10;
        const x = wizard.x + xOffset;
        wizard.buttonText = scene.add.text(x, 780, wizard.button, {
            ...textProps,
            color: colors[wizard.color],
        });

        wizard.sprite = scene.physics.add.sprite(
            x + 80,
            685,
            `${wizard.color}Wizard`,
            0
        );
        wizard.sprite.body.immovable = true;
        wizard.sprite.body.setSize(12, 30, false);
        wizard.sprite.body.setOffset(3, 8);
        wizard.sprite.name = "wizard";
        wizard.sprite.metadata = wizard;
        wizard.sprite.setScale(6);

        /**
         * Animations
         */
        scene.anims.create({
            key: `${wizard.color}Attack`,
            frames: scene.anims.generateFrameNumbers(`${wizard.color}Wizard`, {
                start: 0,
                end: 2,
            }),
            repeat: 0,
            frameRate: 9,
        });
        scene.anims.create({
            key: `${wizard.color}Death`,
            frames: scene.anims.generateFrameNumbers(`${wizard.color}Wizard`, {
                start: 3,
                end: 5,
            }),
            repeat: 0,
            frameRate: 9,
        });
        wizard.sprite.on("animationcomplete", (animation) => {
            if (animation.key === `${wizard.color}Attack`) {
                wizard.sprite.setFrame(0);
            }
        });

        /**
         * Abilities
         */
        scene.wizardAbilityGroup = scene.physics.add.group({
            key: [
                "purpleMainAttack",
                // "blueMainAttack",
                "redMainAttack",
                "orangeMainAttack",
            ],
            max: 0,
            active: false,
            visible: false,
            setXY: {
                x: 10000,
                y: 10000,
            },
        });
        wizardAbilityAnimations[wizard.color](scene);

        /**
         * Keyboard actions
         */
        const key = scene.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes[wizard.button]
        );
        key.on("down", () => {
            if (
                wizard.cooldownTimer &&
                wizard.activeAttack &&
                wizard.activeAttack.texture.key === "orangeMainAttack"
            ) {
                wizard.activeAttack.callback();
                return;
            }
            if (!wizard.alive || wizard.cooldownTimer) {
                return;
            }
            wizardAbilties[wizard.color](scene, wizard);
            wizard.sprite.play(`${wizard.color}Attack`);
        });
    });
};

export const wizardCollisions = (scene) => {
    scene.wizards.forEach((wizard) => {
        scene.physics.add.collider(
            wizard.sprite,
            scene.enemyGroup,
            (wizard, enemy) => {
                wizardCollision(wizard, enemy);
            }
        );
    });
};

export const wizardAbilityCollisions = (scene) => {
    scene.physics.add.collider(
        scene.wizardAbilityGroup,
        scene.enemyGroup,
        (ability, enemy) => {
            abilityCollision(ability, enemy);
        }
    );
};

const wizardCollision = (wizardSprite, enemySprite) => {
    if (enemySprite.state === "death") {
        return;
    }
    enemyDie(enemySprite);
    if (enemySprite.metaData.isUpgrade || !wizardSprite.metadata.alive) {
        return;
    }
    wizardSprite.metadata.alive = false;
    wizardSprite.play(`${wizardSprite.metadata.color}Death`);
    wizardSprite.scene.sfx.wizardDeath();
};

const abilityCollision = (ability, enemy) => {
    if (enemy.state === "death") {
        return;
    }
    enemyDie(enemy, ability.wizard);
    ability.callback();
};

export const allWizardsDead = (scene) => {
    const result = !scene.wizards.reduce((acc, wizard) => {
        if (acc || wizard.alive) {
            acc = true;
        }
        return acc;
    }, false);
    return result;
};

const wizardAbilityAnimations = {
    blue: () => {},
    red: (scene) => {
        scene.anims.create({
            key: "redMainAttack",
            frames: scene.anims.generateFrameNumbers("redMainAttack", {
                start: 0,
                end: 1,
            }),
            repeat: -1,
            frameRate: 12,
        });
    },
    purple: (scene) => {
        scene.anims.create({
            key: "purpleMainAttack",
            frames: scene.anims.generateFrameNumbers("purpleMainAttack", {
                start: 0,
                end: 1,
            }),
            repeat: -1,
            frameRate: 12,
        });
    },
    orange: (scene) => {
        scene.anims.create({
            key: "orangeMainAttack",
            frames: scene.anims.generateFrameNumbers("orangeMainAttack", {
                start: 0,
                end: 1,
            }),
            repeat: -1,
            frameRate: 8,
        });
    },
};

const wizardAbilties = {
    blue: (scene, wizard) => {
        scene.sfx.blueAttack();
        scene.deepFreezeCooldownImage = scene.add.sprite(75, 175, "deepFreeze");
        scene.deepFreezeCooldownImage.setScale(2);
        scene.deepFreezeCooldownMax = wizard.baseAttackSpeed;
        scene.deepFreezeCooldown = wizard.baseAttackSpeed;
        drawDeepFreezeCooldown(scene);
        deepFreezeWizardFlash(scene, wizard);
        setCooldown(scene, wizard);
        scene.spawnTimer += 2000;
        const particles = scene.add.particles("snowflake");
        const x = wizard.sprite.x;
        const y = wizard.sprite.y;
        const snowball = scene.physics.add.sprite(x, y, "snowball", 0);
        particles.createEmitter({
            x,
            y,
            lifespan: 2000,
            speed: 500,
            scale: { start: 1.5, end: 0.5 },
            quantity: 5,
        });
        const tweenSettings = {
            targets: snowball,
            x: 580,
            y: 0,
            duration: 2000,
            repeat: 0,
            onComplete: () => {
                snowball.destroy();
                particles.destroy();
            },
        };
        scene.add.tween(tweenSettings);
        scene.add.tween({
            ...tweenSettings,
            x: 580 - x,
            y: y - 1380,
            targets: particles,
        });
        const enemies = scene.enemyGroup.children.entries;
        const tint = 0xffffff;
        const freezeTime = wizard.baseAttackSpeed;
        enemies.forEach((enemy) => {
            if (
                enemy.state === "death" ||
                enemy.state === "frozen" ||
                !enemy.metaData ||
                enemy.metaData.freezeImmune
            ) {
                return;
            }
            if (enemy.metaData.isUpgrade) {
                enemyDie(enemy, wizard);
            }
            enemy.state = "frozen";
            const { x, y } = enemy.body.velocity;
            // enemy.body.frozenVelocity = { x, y };
            enemy.setFrame(enemy.frame.name + 6);
            enemy.stop();
            enemy.setTint(tint);
            // enemy.tintFill = true;
            enemy.body.velocity.x = 0;
            enemy.body.velocity.y = 0;
            setTimeout(() => {
                if (enemy.state === "death") {
                    return;
                }
                scene.add.tween({
                    targets: enemy,
                    x: enemy.x - 7,
                    y: enemy.y,
                    duration: 1000 / 10,
                    repeat: 10,
                    onComplete: () => {
                        if (enemy.state === "death" || !enemy.body) {
                            return;
                        }
                        enemy.state = 0;
                        enemy.clearTint();
                        enemy.body.velocity.x = x;
                        enemy.body.velocity.y = y;
                        enemy.play(`${enemy.texture}Default`);
                    },
                });
            }, freezeTime - 1000);
        });
        scene.deepFreeze = wizard.baseProjectileNumber;
        scene.deepFreezeSize = wizard.baseProjectileSize - 1;
    },
    red: (scene, wizard) => {
        scene.sfx.redAttack();
        setCooldown(scene, wizard);
        const attacks = [];
        let angleBetween;
        const projectiles =
            wizard.baseProjectileNumber + (scene.deepFreeze ?? 0);
        const projectileSize =
            wizard.baseProjectileSize + (scene.deepFreezeSize ?? 0);
        for (let i = 0; i < projectiles; i++) {
            const attack = scene.wizardAbilityGroup.create(
                wizard.sprite.x + 50,
                wizard.sprite.y - 50,
                "redMainAttack",
                0,
                true,
                true
            );
            const cursorPos = getCursorCenter(scene);
            if (i === 0) {
                angleBetween = Phaser.Math.Angle.BetweenPoints(
                    {
                        x: attack.x,
                        y: attack.y,
                    },
                    { x: cursorPos.x, y: cursorPos.y }
                );
            }
            attack.setScale(projectileSize);
            attack.body.setSize(20, 24);
            attack.body.setOffset(7, 0);
            attack.body.immovable = true;
            attack.callback = () => {
                // attack.destroy();
            };

            const vel = wizard.baseAttackSpeed * scene.baseSpeed;
            let angle;
            if (i === 0) {
                angle = angleBetween;
            } else if (i % 2 === 0) {
                angle = angleBetween - 0.15 * (i / 2);
            } else {
                angle = angleBetween + 0.15 * ((i + 1) / 2);
            }
            const xVel = vel * Math.cos(angle);
            const yVel = vel * Math.sin(angle);
            attack.wizard = wizard;
            attack.body.velocity.x = xVel;
            attack.body.velocity.y = yVel;
            attack.angle = angle;
            attack.setAngle(Phaser.Math.RadToDeg(angle) + 90);

            attacks.push(attack);
        }
    },
    purple: (scene, wizard) => {
        scene.sfx.purpleAttack();
        setCooldown(scene, wizard);
        let remainingProjectiles = 0;
        const projectiles =
            wizard.baseProjectileNumber + (scene.deepFreeze ?? 0);
        const projectileSize =
            wizard.baseProjectileSize + (scene.deepFreezeSize ?? 0);
        const { x: cursorX, y: cursorY } = getCursorCenter(scene);
        do {
            setTimeout(() => {
                const attack = scene.wizardAbilityGroup.create(
                    wizard.sprite.x + 50,
                    wizard.sprite.y - 50,
                    "purpleMainAttack",
                    0,
                    true,
                    true
                );
                attack.wizard = wizard;
                attack.body.immovable = true;
                attack.body.setSize(16, 16);
                attack.body.setOffset(8, 8);
                attack.setScale(projectileSize);
                attack.callback = () => {
                    attack.destroy();
                };
                attack.play(`${wizard.color}MainAttack`);
                attack.movement = scene.physics.moveTo(
                    attack,
                    cursorX,
                    cursorY,
                    wizard.baseAttackSpeed * scene.baseSpeed
                );
            }, 200 * remainingProjectiles);
            remainingProjectiles++;
        } while (remainingProjectiles < projectiles);
    },
    orange: (scene, wizard) => {
        scene.sfx.orangeAttack();
        setCooldown(scene, wizard);
        const attack = scene.wizardAbilityGroup.create(
            wizard.sprite.x + 50,
            wizard.sprite.y - 50,
            "orangeMainAttack",
            0,
            true,
            true
        );
        wizard.activeAttack = attack;
        attack.wizard = wizard;
        attack.body.immovable = true;
        const projectileSize =
            wizard.baseProjectileSize + (scene.deepFreezeSize ?? 0);
        attack.setScale(projectileSize);
        attack.play(`${wizard.color}MainAttack`);
        attack.callback = () => {
            wizardAbilties.orangeSplit(scene, attack, wizard);
        };
        let tween = scene.add.tween({
            targets: wizard.buttonText,
            duration: 400,
            repeat: -1,
            alpha: 0.4,
            yoyo: true,
        });
        tween.destroy = () => {
            if (tween) {
                tween.restart();
                tween.stop();
            }
            tween = null;
        };
        attack.buttonTextTween = tween;
        wizard.buttonTextTween = tween;

        const { x: cursorX, y: cursorY } = getCursorCenter(scene);
        attack.movement = scene.physics.moveTo(
            attack,
            cursorX,
            cursorY,
            wizard.baseAttackSpeed * scene.baseSpeed
        );
    },
    orangeSplit: (scene, mainAttack, wizard) => {
        scene.sfx.orangeSplit();
        wizard.activeAttack = null;
        if (mainAttack.buttonTextTween) {
            mainAttack.buttonTextTween.destroy();
        }
        const { x, y } = mainAttack;
        mainAttack.destroy();
        let attacks = [];
        const numProjectiles =
            wizard.baseProjectileNumber + (scene.deepFreeze ?? 0) * 2;
        const projectileSize =
            wizard.baseProjectileSize + (scene.deepFreezeSize ?? 0);
        for (let i = 0; i < numProjectiles; i++) {
            const attack = scene.wizardAbilityGroup.create(
                x,
                y,
                "orangeMainAttack",
                0,
                true,
                true
            );
            attack.setScale(0.4 * projectileSize);
            attack.wizard = wizard;
            attack.body.setSize(28, 28);
            attack.body.setOffset(0, 0);
            attack.body.immovable = true;
            attack.play(`${wizard.color}MainAttack`);
            attack.callback = () => {
                // attack.destroy();
            };
            const vel = wizard.baseAttackSpeed * 2 * scene.baseSpeed;
            const angle = (360 / numProjectiles) * i;
            const xVel = vel * Math.cos((angle * Math.PI) / 180);
            const yVel = vel * Math.sin((angle * Math.PI) / 180);

            attack.body.velocity.x = xVel;
            attack.body.velocity.y = yVel;

            attacks.push(attack);
        }
    },
};

export const addWizardUpgrade = (enemySprite, wizard) => {
    if (enemySprite.name === "resurrect") {
        return;
    }
    const scene = enemySprite.scene;
    const x = (wizard.numberUpgrades % 5) * 22 + wizard.x + 5;
    const y = Math.floor(wizard.numberUpgrades / 5) * 22 + 932;
    wizard.numberUpgrades += 1;
    const orb = scene.add.sprite(enemySprite.x, enemySprite.y, "upgradeOrb");
    orb.setScale(2);
    scene.add.tween({
        targets: orb,
        x,
        y,
        duration: 800,
        repeat: 0,
        scale: 1,
        ease: "Sine.easeIn",
        onComplete: () => {
            // text.destroy();
        },
    });
};

const setCooldown = (scene, wizard) => {
    wizard.cooldownTimer = wizard.baseCooldown * 1000;
    drawCooldown(scene, wizard);
};

export const drawDeepFreezeCooldown = (scene) => {
    if (scene.deepFreezeCooldownRectangle) {
        scene.deepFreezeCooldownRectangle.destroy();
    }
    if (scene.deepFreezeCooldown > 0) {
        const width =
            64 * (scene.deepFreezeCooldown / scene.deepFreezeCooldownMax);
        scene.deepFreezeCooldownRectangle = scene.add.rectangle(
            scene.deepFreezeCooldownImage.x + width / 2 - 32,
            scene.deepFreezeCooldownImage.y,
            width,
            64,
            0x999999,
            0.6
        );
    } else if (scene.deepFreezeCooldownImage) {
        scene.deepFreezeWizardTween && scene.deepFreezeWizardTween.destroy();
        scene.deepFreezeCooldown = 0;
        scene.deepFreeze = 0;
        scene.deepFreezeSize = 0;
        scene.deepFreezeCooldownImage.destroy();
    }
};

const deepFreezeWizardFlash = (scene, blueWizard) => {
    const wizards = scene.wizards.filter((wizard) => wizard.color !== "blue");
    scene.deepFreezeWizardTween = scene.add.tween({
        targets: wizards.map((wizard) => wizard.sprite),
        duration: 300,
        repeat: -1,
        alpha: 0.5,
        yoyo: true,
    });
    scene.deepFreezeWizardTween.destroy = () => {
        if (scene.deepFreezeWizardTween) {
            scene.deepFreezeWizardTween.restart();
            scene.deepFreezeWizardTween.stop();
        }
        scene.wizards.forEach((wizard) => {
            wizard.sprite.alpha = 1;
        });
        scene.deepFreezeWizardTween = null;
    };
};

export const drawCooldown = (scene, wizard) => {
    if (wizard.cooldownBar) {
        wizard.cooldownBar.destroy();
    }
    const xOff = wizard.button == "W" ? -90 : -100;
    const x = wizard.sprite.x + xOff;
    const y = 790;
    const width = (wizard.cooldownTimer / (wizard.baseCooldown * 1000)) * 120;
    wizard.cooldownBar = scene.add.rectangle(x, y, width, 120, 0xcccccc, 0.6);
    wizard.cooldownBar.setOrigin(0);
};
