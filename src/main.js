import Phaser, { Game } from "phaser";

import TitleScene from "./scenes/TitleScene";
import { MainScene, PausedScene } from "./scenes/MainScene";

const width = 1920 * window.devicePixelRatio;
const height = 1080 * window.devicePixelRatio;

const config = {
    type: Phaser.AUTO,
    backgroundColor: "#000000",
    width,
    height,
    physics: {
        default: "arcade",
        arcade: { debug: false },
    },
    scene: [TitleScene, MainScene, PausedScene],
    globalVars: {},
    pixelArt: true,
    scale: {
        parent: "gameBody",
        mode: Phaser.Scale.ScaleModes.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        zoom: 1 / window.devicePixelRatio,
        width: width * window.devicePixelRatio,
        height: height * window.devicePixelRatio,
    },
};

export default new Phaser.Game(config);
